package com.example.techlab

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    private val buttonMapping = listOf<Pair<Int, Class<*>>>(
        Pair(R.id.button_basic_example, CoroutinesBasicExample::class.java),
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        System.setProperty("kotlinx.coroutines.debug", "on")

        buttonMapping.forEach { map ->
            buttonClickEvent(map.first) {
                startActivity(Intent(this, map.second))
            }
        }
    }

}