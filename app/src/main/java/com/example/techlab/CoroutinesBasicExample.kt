package com.example.techlab

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.*
import kotlin.random.Random

class CoroutinesBasicExample : AppCompatActivity(), CoroutineScope {

    private val tag = "TechLab"

    private val parentJob = Job()

    override val coroutineContext = parentJob + Dispatchers.Main

    // CoroutineScope: Needed for launch, async, cancel... Bind to a specific lifecycle (ViewModel, Activity...)
    // CoroutineContext: Bind to a dispatcher

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coroutines_basic_example)
        title = "Coroutines Basic Example"

        buttonClickEvent(R.id.button_concurrence_proof) {
            concurrenceProof()
        }

        buttonClickEvent(R.id.button_resident_sleeper) {
            residentSleeper()
        }

        buttonClickEvent(R.id.button_produce_values_suspend) {
            sumProducedValues(SumWays.SUSPEND)
        }

        buttonClickEvent(R.id.button_produce_values_async) {
            sumProducedValues(SumWays.ASYNC)
        }

        buttonClickEvent(R.id.button_produce_values_dont_suspend) {
            sumProducedValues(SumWays.DONT_SUSPEND)
        }

        buttonClickEvent(R.id.button_produce_values_dont_suspend_async) {
            sumProducedValues(SumWays.DONT_SUSPEND_ASYNC)
        }
    }

    private fun concurrenceProof() {
        showToast("Starting threads...")

        val testJob = launch {
            displayThreadInfo("NotSpecified           ")
        }

        val childrenJob = parentJob.children.iterator().next()

        Log.i(tag, "parentJob  : ${System.identityHashCode(parentJob)}")
        Log.i(tag, "childrenJob: ${System.identityHashCode(childrenJob)}")
        Log.i(tag, "testJob    : ${System.identityHashCode(testJob)}")
        Log.i(tag, "is testJob equals to childrenJob ? ${testJob === childrenJob}")

//        launch(Dispatchers.Main.immediate)
        launch(Dispatchers.Main) { // Used for UI updates, update LiveData objects
            displayThreadInfo("MainDispatcher         ")
        }

        launch(Dispatchers.Default) { // Heavy CPU work (Sorting lists, parsing JSON...)
            displayThreadInfo("DefaultDispatcher      ")
        }

        launch(Dispatchers.IO) { // Disk or network operations
            displayThreadInfo("IODispatcher           ")
        }

        GlobalScope.launch(Dispatchers.Default) {
            displayThreadInfo("GlobalDefaultDispatcher")
        }

//        lifecycleScope.launchWhenCreated { }
        lifecycleScope.launch(Dispatchers.Default) {
            displayThreadInfo("LCDefaultDispatcher    ")
        }

        // others scopes: viewModelScope
    }

    private fun residentSleeper() {
        launch {
            // Simulates heavy work on main thread
            Thread.sleep(5000L)
        }
    }

    private enum class SumWays { SUSPEND, ASYNC, DONT_SUSPEND, DONT_SUSPEND_ASYNC }

    private fun sumProducedValues(way: SumWays) {
        when (way) {
            SumWays.SUSPEND -> {
                launch(Dispatchers.Default) {
                    val firstValue = produceValue30()
                    val secondValue = produceValue100()

                    val result = firstValue + secondValue

                    showToast("Result is $result") // WRONG

//                    withContext(Dispatchers.Main) {
//                        showToast("Result is $result")
//                    }
                }
            }

            SumWays.ASYNC -> {
                launch(Dispatchers.Main) {
                    val deferredOne = async { produceValue30() } // Deferred = promise of a value
                    delay(randomDelay()) // Do stuff...
                    val deferredTwo = async { produceValue100() }

                    val result = deferredOne.await() + deferredTwo.await()

                    showToast("Result is $result")
                }
            }

            // SUCKS
            SumWays.DONT_SUSPEND -> {
                launch(Dispatchers.Main) {
                    val firstValue = produceValue30DontSuspend()
                    val secondValue = produceValue1000DontSuspend()

                    val result = firstValue + secondValue

                    showToast("Result is $result")
                }
            }

            // SUCKS LESS
            SumWays.DONT_SUSPEND_ASYNC -> {
                launch(Dispatchers.Main) {
                    val deferredValues = listOf(
                        async(Dispatchers.Default) { produceValue30DontSuspend() },
                        async(Dispatchers.Default) { produceValue1000DontSuspend() }
                    )
                    val valuesList = deferredValues.awaitAll()

                    val result = valuesList.sum()

                    showToast("Result is $result")
                }
            }
        }
    }

    private fun displayThreadInfo(alias: String) {
        Log.i(tag, "Alias: $alias - Thread: ${Thread.currentThread().name} (${Thread.currentThread().id})")
    }

    // Thread.sleep vs delay
    private fun produceValue30DontSuspend(): Int {
        Thread.sleep(300L)
        return 30
    }

    private fun produceValue1000DontSuspend(): Int {
        Thread.sleep(1000L)
        return 100
    }

    private suspend fun produceValue30() = withContext(Dispatchers.Default) {
        delay(300L)
        30
    }

    private suspend fun produceValue100() = withContext(Dispatchers.Default) {
        delay(1000L)
        100
    }

    private fun randomDelay() = Random.nextLong(100L, 400L)

}