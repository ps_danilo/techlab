package com.example.techlab

import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity

fun AppCompatActivity.showToast(text: String) {
    Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
}

fun AppCompatActivity.buttonClickEvent(@IdRes resId: Int, onClick: (View) -> Unit) {
    findViewById<Button>(resId)?.setOnClickListener(onClick)
}